<?php
error_reporting(E_ALL);
//error_reporting(1);
//init_set('display_errors','stdout');

define('VERSIONSTATIQUE',1); // attention est valide des que defini (meme si false)
if (defined('VERSIONSTATIQUE')){
	if (VERSIONSTATIQUE == 1){
		error_reporting(0);
		//display_errors(0);
		}
	}

define('SITE_ENGINE_VERSION','sid');
//define('SITE_ENGINE_VERSION','v0.0.2');
$siteVersion_release= array(
     'sid'   => 'version en develloppement'
    ,'v0.1'=> 'un truc basique'
    ,'v0.2'=> 'un truc un peu moins basique'
);

// - docs communs- //
define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INTERSITES_ROOT','./');
define('_0PAGES_ROOT','/www/0pages/');
define('PAGES_ROOT','./pagesLocales/');
define('_0TEXTES_ROOT',DOCUMENT_ROOT.'0textes/');
define('_0ARTICLES_ROOT',DOCUMENT_ROOT.'0articles/');

// - docs specifiques auprojet - //
define('INDEX_ROOT','./');
define('MENU_ROOT','./menus/');
define('PAGESLOCALES_ROOT','./pagesLocales/');

//  === bufferisation ====  //
ob_start();

//  ==== session ==== //
session_name('legral_session');session_start();


//  === gestionnaire de librairies ====  //
require('modeles/_modeleMVC/dev.php');


$lib='gestLib';
include ("./lib/legral/php/$lib/$lib.php");
/* --- appelle d'une version fixe --- //
//include (INTERSITES_ROOT."lib/legral/php/$lib/$lib-v1.0.0.php");
      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
 */

//include ('./lib/legral/php/gestLib/versions/gestLib-v2.0.0.php');
      //$gestLib->libs['gestLib']->setErr(LEGRALERR::DEBUG);

//  ==== Gestion des menus et pages  ==== //
//include('./lib/legral/php/menuStylisee/versions/menuStylisee-v0.1.php');
//include(INTERSITES_ROOT.'lib/legral/php/gestMenus/versions/gestMenus-v1.14.0.php');
//
$lib='menuStylisee';
include("./lib/legral/php/gestMenus/$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$lib='gestMenus';
include("./lib/legral/php/$lib/$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

// - gestionnaire de connexion sql - //
$lib='gestPDO';
include("./lib/legral/php/$lib/$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
$lib='configDB';
//include("./config/$lib.php");
include("./configLocal/$lib.php");

//  ==== Gestion des menus et pages  ==== //
//include('./lib/legral/php/menuStylisee/versions/menuStylisee-v0.1.php');
//include(INTERSITES_ROOT.'lib/legral/php/gestMenus/versions/gestMenus-v1.14.0.php');

$gestMenus=new gestMenus('tutoriels');
//$gestMenus=new gestMenus('about'); // pour test uniquement

$gestMenus->metasPrefixes['title']='legralNet - ';
include('./menus/menus-systeme.php');
include('./menus/menus.php');
//  ==== html ==== //
?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20140531">
<meta name="date-update-yyyymmdd" content="20150417">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">
<?php $gestMenus->build();?>


<!-- scripts concatennes -->
<script src="./locales/scripts.min.js"> </script>
<!-- lib tiers -->
<!-- lib perso -->
<script src="./web/js-vendors/modeleMVC/modeleMVC.js"> </script>

<!-- styles -->
<link rel="stylesheet" href="./styles/styles.css" media="all" />
</head>
<body>
<div id="page">

<!-- header -->
<div id="header">
    <div id="headerGauche"><!--gauche--></div>

    <h1><a href="http://legral.fr/legralDocs">LegralDocs</a>: <a href="?<?php echo $gestMenus->menuDefaut?>">LegralNet</a></h1>

    <div id="headerDroit">
        <?php
    if (SERVER_RESEAU === 'LOCALHOST'){
        echo selectDev();
        echo selectDebug();
    } 
    echo selectVersion($siteVersion_release);
?>

        <a href="?about=accueil">&agrave; propos de...</a>
    </div>
</div><!-- header -->


<!-- menu + ariane + contenu page + ariane -->
<?php
echo '<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";

// - affiche les menus contruit en commencant par le menu indiquee par le constructeur. Inclu la page appellee - //
echo $gestMenus->show();

// -- ariane: affichage-- //
echo "\n".'<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";
include './footer.php';
?><!-- menu + ariane + contenu page + ariane -->

</div><!-- //page -->
</body></html>
