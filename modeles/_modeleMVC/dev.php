<?php

// = ========== = //
// =  DEV / DBG = //
// = ========== = //

// = =========== = //
// =  constantes = //
// = =========== = //


// - DEV_LVL - //
/*
 * utilisation: change le comportement du programme (mode:dev)
 * 0: mode production (utilisation de ./web/locales/js|css
*/
$pspV=0;
if (isset($_GET['DEV_LVL'])){$pspV=(int)$_GET['DEV_LVL'];}
elseif (isset($_GET['ISDEV'])){$pspV=(int)$_GET['ISDEV'];} //retrocompatibilté
elseif (isset($_POST['DEV_LVL'])){$pspV=(int)$_POST['DEV_LVL'];}
elseif (isset($_SESSION['DEV_LVL'])){$pspV=(int)$_SESSION['DEV_LVL'];}
define('DEV_LVL',$pspV);
define('ISDEV',DEV_LVL);//retrocompatibilité
$_SESSION['DEV_LVL']=DEV_LVL;


// - DEBUG_LVL - //
/*
 * utilisation:
 * 0:
*/
$pspV=0;
if (isset($_GET['DEBUG_LVL'])){$pspV=(int)$_GET['DEBUG_LVL'];}
elseif (isset($_POST['DEBUG_LVL'])){$pspV=(int)$_POST['DEBUG_LVL'];}
elseif (isset($_SESSION['DEBUG_LVL'])){$pspV=(int)$_SESSION['DEBUG_LVL'];}
define('DEBUG_LVL',$pspV);
$_SESSION['DEBUG_LVL']=DEBUG_LVL;

unset($pspV);


// = ======= = //
// =  select = //
// = ======= = //

// - function selectVersion - //
// redirige la page vers la version selectionné par le select
function selectVersion($siteVersion_release){
    $o='<select name="selectVersion" id="selectVersion" onchange="reloadOnSelectIdJS(this,\'../\')">';
    $o.='<option disabled>Version</option>';
    foreach ($siteVersion_release as $version => $release){
        $selected=$version===SITE_ENGINE_VERSION?' selected="selected" class="selected" ':'';
        $o.= '<option value="'.$version.'" title="'.$release.'"'.$selected.'>'.$version.'</option>';
    }
    $o.='</select>';
    return $o;
}



// Selectionne le niveau de dev
function selectDev(){?>
<select name="DEV_LVL" id="DEV_LVL" onchange="index=this.selectedIndex;reloadARIANE('DEV_LVL='+this.options[index].value)">
<?php
    $o='<option disabled>DEV</option>';
    for ($i=0;$i<6;$i++){
        $selected=$i===DEV_LVL?' selected="selected" class="selected" ':'';
        $o.= '<option value="'.$i.'" title="'.$i.'"'.$selected.'>'.$i.'</option>';
    }
    $o.='</select>';
    return $o;
}



// Selectionne le niveau de debug
function selectDebug(){?>
<select name="DEBUG_LVL" id="DEBUG_LVL" onchange="index=this.selectedIndex;reloadARIANE('DEBUG_LVL='+this.options[index].value)">
<?php
    $o='<option disabled>DBG</option>';
    for ($i=0;$i<6;$i++){
        $selected=$i===DEBUG_LVL?' selected="selected" class="selected" ':'';
        $o.= '<option value="'.$i.'" title="'.$i.'"'.$selected.'>'.$i.'</option>';
    }
    $o.='</select>';
    return $o;
}


// = ================= = //
// =  fonctiond de dev = //
// = ================= = //
// http://php.net/manual/en/function.opendir.php
function profile( $func, $trydir){
    $mem1 = memory_get_usage();
    echo '<pre>----------------------- Test run for '.$func.'() ...'."\n";
    flush();

    $time_start = microtime(TRUE);
    $list = $func( $trydir);
    $time = microtime(TRUE) - $time_start;

    echo 'Finished : '.count($list).' files</pre>';

    $mem2 = memory_get_peak_usage();

    printf( '<pre>Max memory for '.$func.'() : %0.2f kbytes Running time for '.$func.'() : %0.f s</pre>',($mem2-$mem1)/1024.0, $time);
    return $list;
}

