<?php
ob_start();
define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INDEX_ROOT',dirname(__FILE__).'/');
define('INTERSITES_ROOT','./');

//define('_0PAGES_ROOT',INDEX_ROOT.'pages/');
#define('_0TEXTES_ROOT',INDEX_ROOT.'0textes/');
#define('_0ARTICLES_ROOT',INDEX_ROOT.'0articles/');

define('PAGES_ROOT','/www/0pages/');
//echo 'PAGES:'.PAGES_ROOT.'<br>';

define('VERSIONSTATIQUE',1); // attention est valide des que defini (meme si false)

//include (INTERSITES_ROOT.'lib/perso/php/intersites/intersites-3.0.php');
include (INTERSITES_ROOT.'lib/legral/php/gestLib/gestLib-0.1.php');
include (INTERSITES_ROOT.'lib/legral/php/menuStylisee/menuStylisee-v0.1.php');


//*********** ==== Gestion des pages et des menus ==== *****************

//include(INTERSITES_ROOT.'lib/legral/php/gestMenus/gestMenus-v0.1.10.php');
//include(INTERSITES_ROOT.'lib/legral/php/gestMenus/gestMenus-v1.11.php');
//include(INTERSITES_ROOT.'lib/legral/php/gestMenus/gestMenus-v1.12.php');
include(INTERSITES_ROOT.'lib/legral/php/gestMenus/gestMenus-v1.13.php');

$gestMenus=new gestMenus('tutoriels');
//$gestMenus=new gestMenus('about'); // pour test uniquement

$gestMenus->metasPrefixes['title']='legralnet - ';
include('./menus/menus-systeme.php');
include('./menus/menus.php');
include('./menus/menus-langages.php');
include('./menus/menus-bootLoader.php');
include('./menus/menus-linux.php');
include('./menus/menus-debian.php');
include('./menus/menus-debianLive.php');

?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20140531">
<meta name="date-update-yyyymmdd" content="20140531">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">
<?php $gestMenus->build();?>

<link rel="stylesheet" href="./styles/styles.min.css" media="all" />
<!--
<link rel="stylesheet" href="./lib/legral/css/knacss/knacss.css" media="all" />
<link rel="stylesheet" href="./lib/legral/php/gestLib/gestLib.css" media="all" />
<link rel="stylesheet" href="./styles/intersites.css" media="all" />
<link rel="stylesheet" href="./styles/html4.css" media="all" />
<link rel="stylesheet" href="./styles/html4-local.css" media="all" />
<link rel="stylesheet" href="./lib/legral/php/gestLib/gestLib.css" media="all" />
-->
<link rel="stylesheet" href="./styles/notes/notes.css" media="all" />
<link rel="stylesheet" href="./styles/tutoriels/tutoriels-style.css" media="all" />
<link rel="stylesheet" href="./lib/legral/php/menuStylisee/styles/menuOnglets-defaut/menu.css" media="all" />

<style></style>
</head>
<body>
<div id="page">
<div id="header">
<div id="headerGauche"><!--gauche--></div>
<h1><a href="http://legral.fr/legralDocs">LegralDocs</a>: <a href="?<?php echo $gestMenus->menuDefaut?>">LegralNet</a></h1>

<div id="headerDroit"><a href="?about=accueil">&agrave; propos de...</a></div>
</div>

<?php
echo '<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";

// - affiche les menus contruit en commencant par le menu 'tutoriels' - //
echo $gestMenus->show();

// -- ariane: affichage-- //
echo "\n".'<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";

?>
<div id="footer">
<div id="footerGauche"><?php if(defined('VERSIONSTATIQUE')){
	echo 'version statique(g&eacute;n&eacute;r&eacute;e le: '.date('d/m/Y').')';
	}
?></div>
<?php echo $gestMenus->getLastTitre().'<br>';   //afficher le titre de la page?>
<span class="licence">
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Licence Creative Commons" src="./styles/img/licenceCCBY-88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">legralNet</span> de <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Fenwe</span> est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">licence Creative Commons Attribution 4.0 International</a>.<br />
Fond&eacute; sur une œuvre &agrave; <a xmlns:dct="http://purl.org/dc/terms/" href="http://legral.fr" rel="dct:source">http://legral.fr</a>.</span>
<div id="footerDroit">droite</div>
<?php @include DOCUMENT_ROOT.'piwik.php';?></body></html>
</div><!-- footer -->

<?php //include (PAGES_ROOT.'_site/inspect.php'); ?>


</div><!-- //page -->

