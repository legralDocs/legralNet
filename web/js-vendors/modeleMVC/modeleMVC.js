// ################## # //
// # gestion des divs # //
// ################## # //

function inlineSwitch(idCSS){
	        IDCSSS=document.getElementById(idCSS).style;IDCSSS.display=(IDCSSS.display=='none')?'inline':'none';
}
function blockSwitch(idCSS){
	        IDCSSS=document.getElementById(idCSS).style;IDCSSS.display=(IDCSSS.display=='none')?'block':'none';
}

function blockVisible(idCSS){
	        IDCSSS=document.getElementById(idCSS).style;IDCSSS.display='block';
}

function blockNone(idCSS){
	        IDCSSS=document.getElementById(idCSS).style;IDCSSS.display='none';
}

/* 2 fonctions pour ajuster automatiquement la hauteur d'un textarea
// source: http://www.solucior.com/fr/3-Ajuster_automatiquement_la_hauteur_d_un_textarea.html
*/
function IDhautAuto(idtCSS) {
   if (document.getElementById(idtCSS).scrollTop > 0) IDhautAug(idtCSS);
}
function IDhautAug(idtCSS) {
	var idJS=document.getElementById(idtCSS);
	if(idJS.style.height=='')idJS.style.height='0';
	var h = parseInt(idJS.style.height);
	idJS.style.height = h + 10 +"px";
	IDhautAuto(idtCSS);
}



// ##################### # //
// # gestion des selects # //
// ##################### # //

// - Recharge la page selon la valeur du select 'selectIdJS' - //
function reloadOnSelectIdJS(selectIdJS,dir){
    if (dir === undefined) dir='./';
    index=selectIdJS.selectedIndex;
    location.href=dir+selectIdJS.options[index].value;
}

// - Recharge la page vers ARIANE et le psp precisé - //
function reloadARIANE(psp){
    location.href='?'+psp;
}

