<?php
/*!******************************************************************
fichier: gestMenus.php
version : voir declaration gestLib
auteur : Pascal TOLEDO
date de creation:31 mai 2014
date de modification: 12 juillet  2014
source: http://www.legral.fr/intersites/lib/perso/php/gestMenu
gitorious:

depend de:
* gesLib.php
* menuStylisee.php
* 
description:

tutoriel:
// ordre de surcharge1 est surcharge par 2 qui est surcharge par 3 etc
// 1- session	// $pageLast_session
// 2- cookie	// $pageLast_cook
// 3- get	// $pageLast_Get

// reparation de fortune apres avoir tout cassé avec git et la branche ajoutMachin

***********************************************************************/
$gestLib->loadLib('gestMenus',__FILE__,'1.2.0-dev',"gestionnaire de menus");
$gestLib->libs['gestMenus']->git='https://git.framasoft.org/legraLibs/gestMenus';
//	$gestLib->lib['gestMenus']->setErr(LEGRALERR::DEBUG);

/***********************************************************************
Convertie les caracteres speciaux en leurs equivalents
***********************************************************************/
function convertAccent($text){
	//$ext=htmlentities($text,ENT_NOQUOTES,'UTF-8');$text=htmlspecialchars_decode($text);return $text;
	$carSpec=array('é','è','à','â','ô','î','ç');
	$htmlSpec=array('&eacute;','&egrave;','&agrave;','&acirc;','&ocirc;','&icirc;','&ccedil;');
	return str_replace($carSpec,$htmlSpec,$text);
	}
function slasheQuote($text){
	$carSpec=array("'",'"');
	$htmlSpec=array("\'","\"");
	return str_replace($carSpec,$htmlSpec,$text);
}

function ln2br($txt){
    return str_replace(array("\r\n", "\r", "\n"), "<br />", $txt);
}

// Originally written by xellisx
// http://php.net/manual/fr/function.parse-url.php
function parse_query($var){
  /**
   *  Use this function to parse out the query array element from
   *  the output of parse_url().
   */
  $var  = parse_url($var, PHP_URL_QUERY);
  $var  = html_entity_decode($var);
  $var  = explode('&', $var);
  $arr  = array();

  foreach($var as $val){
    $x          = explode('=', $val);
    $arr[$x[0]] = $x[1];
   }
  unset($val, $x, $var);
  return $arr;
}
/***********************************************************************
class gestMeta
Sauvegarde dans une pile des menus successives avec leur page respective.
***********************************************************************/
class gestMetas{
//	public  $titlePrefixe=''; // prefixea ajoute au meta html title
	private $meta=array();    // meta connu:  leur code sera ajouter. Seule la valeur doit etre renseigner (tittle,refresh) eg: title="mon titre" -> <title>mon titre</title>
	private $metaFull=array();	// metas non connu: le code sera ecrit tel quel dans le <head>

	function __construct(){
		$this->meta['title']='';	// doit etre defini pour afficher la balise meta (meme si vide)
		$this->meta['unique']='<meta name="test" content="pour test only"/>';
		}
	
	function shows(){return $this->showMeta().$this->showMetaFull();}

	function getMeta($meta){return (iseet($this->meta[$meta])?$this->meta[$meta]:'');}
        function setMeta($meta,$val){$this->meta[$meta]=$val;}

        function showMetas($prefixe=[]){
		$titlePrefixe='';
		if(is_array($prefixe)){
			//echo "prefixe defini\n";
			//echo gestLib_inspect('prefixe',$prefixe);
			if(isset($prefixe['title'])){$titlePrefixe=$prefixe['title'];/*echo "titlePrefixe modifier\n";*/}
			}
                $out='<!-- metas specifique -->';
		foreach($this->meta as $k =>$v){
                        switch($k){
                                case 'refresh':$out.="";break;
                                case 'title':$out.="<title>{$titlePrefixe}$v</title>";//echo "htmltitle afficher\n";break;
                                case 'favicon':$out.='<link rel="icon" href="'.$v.'" />';break;
                                default:$out.=$this->meta[$k]."\n";
                                }
                        }
                return $out;
                }

        function getMetaFull($meta){return(iseet($this->metaFull[$meta])?$this->metaFull[$meta]:'');}
        function setMetaFull($meta,$val){$this->metaFull[$meta]=$val;}
        function showMetaFull(){$out='<!-- metas complet -->';foreach($this->metaFull as $v)$out.=$v;  return $out;}
	}


/***********************************************************************
class gestAriane
Sauvegarde dans une pile des menus successives avec leur page respective.
***********************************************************************/
class gestAriane
	{
	private $pileNb=0;
	private $lastMenu='';
	private $lastPage='';
	public  $arianeGet='';	// affiche l'ariane sous forme menu1=page1&amp;menu2=page2&amp;etc
	public  $pile=array();
	// -- -- //
	function __construct(){}

	function __toString(){
		$out='';
		for($i=0;$i<$this->pileNb;$i++)$out.='wmenu:'.$this->pile[$i]['menu'].'; page:'.$this->pile[$i]['page'].'<br>';
		return $out;
		}
	function addMenu($menu,$page)
		{
		if(!$menu){return -1;}
		$this->pile[$this->pileNb]['menu']=$menu;
		$this->pile[$this->pileNb]['page']=$page;
		$this->lastMenu=$menu;
		$this->lastPage=$page;
		$this->arianeGet.="&amp;menu=$page";
		$this->pileNb++;
		}

	public function getlastMenu(){return $this->lastMenu;}
	public function getlastPage(){return $this->lastPage;}


	// -- renvoie la chaine ariane -- // 
	function showAriane(){
		$href='?';
		$out='';
		for($m=0;$m<$this->pileNb;$m++){
			$menu=$this->pile[$m]['menu'];
			$page=$this->pile[$m]['page'];
			$href.="$menu=$page&amp;";
			$out.='-&gt;';
			$out.="<a href='$href'title='page:$page'>$menu</a>";
			}
		return "$out\n";
		}

	// -- retourne ?menu1=page1&amp;menu2=page2&amp;menumenuLvl=pagemenuLvl&amp;-- //
	// si menuLvl===NULL: renvoie les tous les niveaux sinon renvoie jusqu'au niveau spécifié
	function getHref($menuLvl=NULL,$pspStart='?'){
		global $gestLib;
		if($menuLvl===NULL)$menuLvl=count($this->pile)+1;		
		//echo $gestLib->debugShowVar('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$menuLvl',$menuLvl);
		$href=$pspStart;
		echo $gestLib->debugShowVar('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$href',$href);
               for($m=0;$m<$menuLvl;$m++)
                        {
                        $menu=(isset($this->pile[$m]['menu']))?$this->pile[$m]['menu']:'';
                        $page=(isset($this->pile[$m]['page']))?'='.$this->pile[$m]['page']:'';
                        if("$menu$page"!='')$href.="$menu$page&amp;";
			//echo $gestLib->debugShowVar('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$href',$href);

                        }
                return "$href";
		}

	}

// - *********************************************************************** - //
// - gestionnaire d'appel des pages - //
// - *********************************************************************** - //
class gestCallPage
	{
	private $cssLI='';      // liste des classes css qui sera ajoute au code <li> (eg:'margin')
	private $cssA='';       // liste des classes css qui sera ajoute au code <a>  (eg:'underline')
	private $inlineLI='';
	private $inlineA='';
	private $span='';	// html: <span>$span <a></a>  </span>

	private $attr=array();	// parametres de la page
					// URL:
					// visible:
					// menuTitre: titre qui sera afficher
	private $get=array();	// sert de code dans une url (eg: dans ?menu1=page2, get=menu1)
	public  $metas=NULL;	// gestionnaire de metas voir gestMetas

	function __construct($url)
		{
		$this->attr['URL']=$url;
		$this->attr['visible']=1;	// par defaut: visible
		$this->metas=new gestMetas();
		}

        function __toString(){
                //return __CLASS__.gestLib_inspect($this).'<br>';
                }

	function getAttr($attrNom){return(isset($this->attr[$attrNom])?$this->attr[$attrNom]:NULL);}
	function setAttr($attrNom,$attrVal){if($attrNom){$this->attr[$attrNom]=$attrVal;}}

	function getGet($getNom){return(isset($this->get[$getNom])?$this->get[$getNom]:NULL);}
	function setGet($getNom,$getVal){if($getNom){$this->get[$getNom]=$getVal;}}

	
		//-  manipulation des attributs de la page - //

	// -- getMenuTitle: renvoie $menuTitle si non definie renvoie le titre le definit le plus proche -- //
	function getMenuTitle(){
		return (isset($this->attr['menuTitle']))?$this->attr['menuTitle']
		     : (isset($this->attr['menuTitre']))?$this->attr['menuTitre']
		     : (isset($this->attr['titre']    ))?$this->attr['titre']
		     : $this->attr['URL'];
		     ;
	}

	 //-  manipulation des classes des sous composants du template menuStylisee:onglet<li><a> - //
	function getCssLI(){return $this->cssLI;} function setCssLI($classes){$this->cssLI=$classes;} function addCssLI($classes){$this->cssLI.=" $classes";}
	function getCssSP(){return $this->cssSP;} function setCssSP($classes){$this->cssSP=$classes;} function addCssSP($classes){$this->cssSP.=" $classes";}
	function getCssA (){return $this->cssA;}  function setCssA ($classes){$this->cssA =$classes;} function addCssA ($classes){$this->cssA .=" $classes";}

	function getInlineA (){return $this->inlineA;}  function setInlineA ($inlineA) {$this->inlineA =$inlineA;}  function addInlineA ($inlineA) {$this->inlineA .=" $inlineA";}
	function getInlineLI(){return $this->inlineLI;} function setInlineLI($inlineLI){$this->inlineLI=$inlineLI;} function addInlineLI($inlineLI){$this->inlineLI.=" $inlineLI";}

	function getSpan(){return $this->span;}  function setSpan($html) {$this->span=$html;}  function addSpan($html) {$this->span .=" $html";}


}	//class gestCallPage
	
// -- *********************************************************************** -- //
// -- gestion d'un menu-- //
// -- un menu gere des pages -- // 
// -- *********************************************************************** -- //
class gestMenu
	{
	public  $gestNom=NULL;		// nom du gestionnaire
	public  $pageLast=NULL;
	public  $pageDefaut=NULL;	// page par defaut
	private $URL_defaut=NULL;
	public  $pageNomActuelle=NULL;
	public  $varGet=NULL;           // nom de la var utiliser dans get ex: ?page=

	public  $template='onglets';	// genre du template 
	public  $classes='ms';		// classes de base pour tout le menu (LIs)

	public  $metas=NULL;		// gestionnaire de metas pour le menus lui-meme
	public  $callPages=array();		// pages (attributs)page[page]->attr


	function __construct($nom,$pageDefaut='',$URL_defaut='',$varGet=NULL)
		{
		$this->gestNom=$nom;
		$this->varGet=$varGet?$varGet:$nom;
		$this->pageDefaut=$pageDefaut;
		$this->URL_defaut=$URL_defaut;

		$this->pageNomActuelle=$this->pageDefaut;	$this->pageLast=$this->pageDefaut;

		$this->addCallPage($pageDefaut,$URL_defaut);	// supprimer car appellais un
		$this->setPageLast();
		$this->setPageNomActuelle();
		
//		if($pageDefaut==''){
//				$this->setAttr($pageDefaut,'visible',0);
//			}


		}	

	// - renvoyer les donnees de l'object si demande d'affichage - //
        function __toString(){
		global $gestLib;
                $out=__CLASS__;
		//foreach($this as $key => $value)  $out.=$gestLib->inspect($key,$value);
		$out.=$gestLib->inspect($this);
                return $out;
                }

	//-  addClasses: ajoute une ou plusieurs classe a integré a toutes les pages  - //
	public function addClasses($classes){$this->classes.=" $classes";}

	//-  addClassesInPage: ajoute une ou plusieurs classe a integré a une page  - //
	public function addClassesInPage($page,$classes){if(isset($this->$pages[$page]))$this->$pages[$page]->addClasses($classes);}


	public function getPageLast(){
		return $this->pageLast;}

	public function setPageLast($newPageLast=NULL){
		global $gestLib;
		if(isset($_COOKIE[$this->gestNom.'_pageNomActuelle'])){$this->pageLast=$_COOKIE[$this->gestNom.'_pageNomActuelle'];}
		if($newPageLast){$this->pageLast=$newPageLast;}
		setcookie($this->gestNom.'_pageLast',$this->pageLast,(time()+3600*24*365));

		// - rajouter ici le code js a ecrire dans le head - //

		}

	public function addCallPage($pageNom,$url){
		// - si table de page vide et pas de page par defaut specifier alors cette 1ere page est la page par defaut - //
		if( (count($this->callPages)==0) and ($this->pageDefaut=='') ){
			$this->pageNomActuelle=$pageNom;
			}
		$this->callPages[$pageNom]=new gestCallPage($url);}

	public function getPageNomActuelle(){
		return $this->pageNomActuelle;}

	public function setPageNomActuelle($page=NULL){
		global $gestLib;
		if($this->pageLast){$this->pageNomActuelle=$this->pageLast;}
		if(isset($_GET [$this->varGet])){$this->pageNomActuelle=$_GET [$this->varGet];}
		if(isset($_POST[$this->varGet])){$this->pageNomActuelle=$_POST[$this->varGet];}
		if($page){$this->pageNomActuelle=$page;}
		setcookie($this->gestNom.'_pageNomActuelle', $this->pageNomActuelle,(time()+3600*24*365));
		}
	public function getURL($page=NULL)
		{
		$page=$page?$page:$this->pageNomActuelle;
		if(@$this->callPages[$page]){$url=$this->callPages[$page]->getAttr('URL');return $url?$url:$this->URL_defaut;}//accee direct 'pages[$page]->getAttr' oblige!
		return NULL;
		}

	//-- Ouvre un fichier et renvoie le texte avec les accents convertit -- //
	private function parserPage($url){
		$out='';if($fd=@fopen($url,'r')){while(!feof($fd)){$buffer=fgets($fd,4096);$out.=$buffer;}fclose($fd);}return convertAccent($out);}

	public function incPage($page=NULL){
		global $gestLib;
		$page=$page?$page:$this->pageNomActuelle;
		$url=$this->getURL($page);
		if($url==NULL){$url=$this->URL_defaut;}
		if(file_exists($url)){
			echo $gestLib->debugShow('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'le fichier existe -&gt; inclusion('.$url.')');
		}
//		elseif($this->getURL(404)){$url=$this->getURL(404);echo '404 appeller!';}
		//	else{$url=$this->URL_defaut;}
		//echo '<div id="pageContent">';// l'id est en dur
		if(substr($url,-4)=='.php'){include $url;}else{echo $this->parserPage($url);}//si .php inclure sinon parser pour accent
		//echo '</div><!--div id="pageContent"-->';
	}
	
	public function showGet($page=NULL){
		return(isset($this->callPages[$page]))?$this->callPages[$page]->showGet():NULL;}

	// --- Accee aux attr des pages --- //
	public function setAttr($page,$attrNom,$attrVal){
		if(isset($this->callPages[$page]) AND $attrNom){$this->callPages[$page]->setAttr($attrNom,$attrVal);}}

	public function getAttr($page,$attrNom){
		if($attrNom=='URL'){return $this->getURL($page);}
		return isset($this->callPages[$page])?$this->callPages[$page]->getAttr($attrNom):NULL;
		}

	public function setGet($page,$getNom,$getVal){
		if(isset($this->callPages[$page]) AND $getNom){$this->callPages[$page]->setGet($getNom,$getVal);}}

	public function getGet($page,$getNom){
	     return isset($this->callPages[$page])?$this->callPages[$page]->getAttr($attrNom):NULL;}

	public function toArray($attr=NULL,$attrVal=NULL)
		{$out=array();
		if(!$attr)foreach($this->callPages as $key => $value){$out[]=$key;}
		elseif(!$attrVal)//
			{
			foreach($this->calPages as $key => $value){if ($value->getAttr($attr)){$out[]=$key;}}
			}
		else	{
			foreach($this->callPages as $key => $value){if ($value->getAttr($attr)==$attrVal){$out[]=$key;}}
			}
		return $out;
		}
/*
	// --- renvoie TRUE si la page a l'attribue ssMenu --- //
	function isPagemenu($page){
		if( (isset($this->calPages[$page])) AND  ($this->callPages[$page]->getAttr('ssMenu')===1) )return TRUE;
		return FALSE;
		}
*/

	// -- manipuler les meta des pages-- //
	function meta_shows($page){if(isset($this->callPages[$page]))return $this->callPages[$page]->meta->shows();}

        function getMeta($page,$meta)         {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->getMeta($meta);}
        function setMeta($page,$meta,$val)    {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->setMeta($meta,$val);}

        function showMeta($page)              {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->showMeta();}
  
        function getMetaFull($page,$meta)     {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->getMetaFull($meta);}

        function setMetaFull($page,$meta,$val){if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->setMetaFull($meta,$val);}

        function showMetaFull($page)          {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->showMeta();}


	// -- manipuler les css des pages-- //
	function getCssLI($page)             {if(isset($this->callPages[$page])) return $this->callPages[$page]->getCssLI();}
	function setCssLI($page,$css)        {if(isset($this->callPages[$page])) return $this->callPages[$page]->setCssLI($css);}
	function addCssLI($page,$css)        {if(isset($this->callPages[$page])) return $this->callPages[$page]->addCssLI($css);}	

	function getCssA ($page)             {if(isset($this->callPages[$page])) return $this->callPages[$page]->getCssA ();}
	function setCssA ($page,$css)        {if(isset($this->callPages[$page])) return $this->callPages[$page]->setCssA ($css);}
	function addCssA ($page,$css)        {if(isset($this->callPages[$page])) return $this->callPages[$page]->addCssA ($css);}

	function getInlineLI($page)          {if(isset($this->callPages[$page])) return $this->callPages[$page]->getInlineLI();}
	function setInlineLI($page,$html)    {if(isset($this->callPages[$page])) return $this->callPages[$page]->setInlineLI($html);}
	function addInlineLI($page,$html)    {if(isset($this->callPages[$page])) return $this->callPages[$page]->addInlineLI($html);}	

	function getInlineA ($page)          {if(isset($this->callPages[$page])) return $this->callPages[$page]->getInlineA ();}
	function setInlineA ($page,$html)    {if(isset($this->callPages[$page])) return $this->callPages[$page]->setInlineA ($html);}
	function addInlineA ($page,$html)    {if(isset($this->callPages[$page])) return $this->callPages[$page]->addInlineA ($html);}
	
	function getSpan ($page)             {if(isset($this->callPages[$page])) return $this->callPages[$page]->getSpan ();}
	function setSpan ($page,$html)       {if(isset($this->callPages[$page])) return $this->callPages[$page]->setSpan ($html);}
	function addSpan ($page,$html)       {if(isset($this->callPages[$page])) return $this->callPages[$page]->addSpan ($html);}

	// -- return le menu selon le tempate -- //
	function showMenu($gestMenus){
		$out='';
		switch ($this->template)
			{

			case'css': CASE NULL:	echo ('NULLLLLLLLL');break;

			case'ms':case'ms:onglets':
			default:
				// -- cration de l'onglet -- //
				$onglets=new menuStylisee($this->classes);
				foreach ($this->toArray('visible',1) as $key => $pageIndex){
					//-- personalisation de l'onglet -- //
					$titre=$this->getAttr($pageIndex,'menuTitre');
					$getHref=$gestMenus->ariane->getHref($gestMenus->menuLvl);
					$getHref.=$this->varGet.'='.$pageIndex;

					$onglets->addLi($pageIndex,$titre,$getHref);
						$onglets->li[$pageIndex]->setVarGet($pageIndex);
						$onglets->li[$pageIndex]->addInlineLI($this->callPages[$pageIndex]->getInlineLI ());// ajoute $inlineLI dans le code html de <li>
						$onglets->li[$pageIndex]->addCssLI($this->callPages[$pageIndex]->getCssLI());   // ajouter les classes affectes au <li>
						$onglets->li[$pageIndex]->addSpan ($this->callPages[$pageIndex]->getSpan());   // 
						$onglets->li[$pageIndex]->addCssA ($this->callPages[$pageIndex]->getCssA ());   // ajouter les classes affectes au <a>
						$onglets->li[$pageIndex]->addInlineA ($this->callPages[$pageIndex]->getInlineA ());// ajoute $inlineA  dans le code html de <a>
					}// foreach
				$out.=$onglets->show($this->getPageNomActuelle()); // afficher l'onglet et mettre en surbrillance l'onglet en parametre
				break;
			}
		return $out;
		} // showMenu()
	} // class gestMenu


// -- *********************************************************************** -- //
// -- metaclass de gestion DES menuS -- //
// gere l'historique global, les meta html , l'ajout des menus via le parse du get
// -- *********************************************************************** -- //
class gestMenus{
	public  $menuDefaut='';          // menu qui sera appelle en 1er (ou si aucun menu est appelle)

	public  $menuLvl=1;		// nombre de niveau de menu appellé (pour les li)
        public  $template='ms:onglets';	// style du template par defaut: ms (autre possible: aucun //onglet,select,UL)

	// - resultat de build - //
	private $_pageSelect='';	// page (qui n'est pas un menu) trouvé et sauvé par build; à usage interne et intermediare.
	private $_buildData='';		// contenu du menu contruit
	public  $isCall=0;		// la menu at'il été appellé via le query?

	private $metas;                 // se connecte sur un gestionnaire des metas html (et title) d'une page (n'instancie aucun gestionnaire)
	public  $metasPrefixes=array();  // prefixe ajoute au meta html title (pour gestMetas)

	// - gestion de pageContent - //

	// -- gestion de l'affichage de pageContent -- //
	// 0: la page n'est jamais afficher
	// 1: auto (si le nom du menu est dans le query)
	public  $showPage=1;	
	
	public  $pageContentShow=1;		// le contenu de la pages sera inséré après le menu. Permet de cumuler des menus successif
	public  $pageContentId='pageContent';

	public  $ariane=NULL;           // ariane des pages actives
	public  $arianeFull='';
	public  $menus=array();         // tableau de gestionnaire de menu (gestMenu)
//	public  $pages=NULL;            // future tableau de gestionnnaire de page (a creer)


	// - - //
	//
	function __construct($menuDefaut)
                {
		$this->ariane=new gestAriane();
		$this->metas=NULL;		// n'intancie pas le gestionnaire mais se connecte sur un autre
		$this->menuDefaut=$menuDefaut;  // menu qui sera en premier
                }

        // - renvoyer les donnees de l'object si demande d'affichage - //
        function __toString(){
                global $gestLib;
                $out=__CLASS__.'<br>';
		foreach($this as $key => $value)  $out.=gestLib_inspect($key,$value);
		//$out.=$gestLib->inspect($this);	// 500:internal server error // NE PAS SUPPRIMER CETTE LIGNE
                return $out;
                }

	// - renvoie la page par defaut du menu par defaut (utile quand pas de psp) - //
	function getPageDefaut(){
		return $this->menus[$this->menuDefaut]->pageDefaut;
	}

	// - renvoie le titre de la derniere page d'ariane- //
	function getLastTitre(){
		$menu=$this->ariane->getlastMenu();
		$page=$this->ariane->getlastPage();
		return isset($this->menus[$menu]->callPages[$page])?$this->menus[$menu]->callPages[$page]->getAttr('titre'):'';
		}

	function getLastMenuTitle(){
                $menu=$this->ariane->getlastMenu();
                $page=$this->ariane->getlastPage();
		return isset($this->menus[$menu]->callPages[$page])?$this->menus[$menu]->callPages[$page]->getMenuTitle():'';
                }


	// - ajoute un menu dasn le tableau des menus - //
	// - $menuTitre,$titre,$varGet='',$pageDefaut NON IMPLEMENTER  - //
	function addMenu($menuNom,$pageDefaut,$URLDefaut){
		 //                                   ($nom,$pageDefaut,$URL_defaut,$varGet=NULL)
		$m=$this->menus[$menuNom]=new gestMenu($menuNom,$pageDefaut,$URLDefaut);
		//if($varGet!=='')$m->varGet=$varGet;
		return $m;
		}

	// -- genere les menus et sauve la page selectionnée-- //
	function build($showMeta=1){
		global $gestLib;
		$qs=$_SERVER['QUERY_STRING'];$str=array();parse_str($qs,$str);
		$menuOut='';
		$m=$p='';
		foreach($str as $m => $p){

			// **** reorganisation debut  **** //

			if(!isset($this->menus[$m]))continue; // ce psp n'est pas un menu il ne regarde donc pas ce programme

			// - gestion de la partie menu du psp - //

			// -- on ne prend que son 1er appel -- //
			// le menu est appelle n fois: une fois par le menu et une fois par la page des menus precedents
		        if($m!=$this->ariane->getLastMenu())
		                {
				$this->menuLvl++;

				// -- maj d'ariane -- // --> il ne devrait pas etre la mais dans $this->addMenu() a tester
				$this->ariane->addMenu($this->menus[$m]->gestNom,$this->menus[$m]->getPageNomActuelle());

				// -- construction et lecture de ce menu -- //
				$menuOut.=$this->menus[$m]->showMenu($this);

				// -- initialisation du gestionnaire terminal des metas -- //
				$this->metas=$this->menus[$m]->metas;
				}

			// - gestion de la partie page du psp(query) - //
			// -- la page est il un menu? (la page a appelle un autre menu) -- //
			if(isset($this->menus[$p]))
		                {       
				$this->isCall=1;

				//echo "la page $p est un menu -> on ajoute ce menu<br>";

				// -- maj d'ariane -- //
	 			$this->ariane->addMenu($this->menus[$p]->gestNom,$this->menus[$p]->getPageNomActuelle());

				// -- construction et lecture de ce menu -- //
				$menuOut.=$this->menus[$p]->showMenu($this);

				// -- initialisation du gestionnaire terminal des  metas -- //
				$this->metas=$this->menus[$p]->metas;
				}       
			else{// echo "$p n'est pas un menu)<br>";
				// -- est-ce une page valide ? -- //
				if(isset($this->menus[$m]->callPages[$p])){
					$this->isCall=1;

					// -- initialisation du gestionnaire final des  metas -- //
					$this->metas=$this->menus[$m]->callPages[$p]->metas;

					// --- mise a jours  de metaTitle avec le titre de la page --- //
					$pageTitre=$this->menus[$m]->callPages[$p]->getAttr('titre');
					//echo gestLib_inspect('getTitre',$pageTitre).'<br>';
					$this->metas->setMeta('title',$pageTitre);
					}
				}	
			} // foreach

		// -- sauvegarde des valeurs-- // 
		//define('ARIANE', $this->ariane->getHref(NULL,''));//''=pas de PSPstart
		$this->arianeFULL=$this->ariane->getHref(NULL,'');//''=pas de PSPstart


		$this->_buildData="$menuOut\n";
		$this->_pageSelect=$m;

		// ---- debug ---- //
		if(empty($m)){
			$this->isCall=1;$this->showPage=1;		// si pas de page appeller ou concordante forcer l'appelle
			$this->_pageSelect=$this->menuDefaut;		// du menu par defaut
			//echo gestLib_inspect('gestMenus:$_pageSelect',$this->_pageSelect);
		}



		// -- affichage des metas -- //
        $out='';
        if($showMeta){
			// recupere les metas de la page selectionnée par le dernier menu
			$out.= '<!-- meta specifique -->';
			if($this->metas instanceof gestMetas){
				$out.= $this->metas->showMetas($this->metasPrefixes);
				}
			else $out.= '<!-- aucun gestionnaire de meta instancie car rien a heriter -->';
		}
    return $out;
	} //function build($showMeta=1)

		
	// - affiche le menu - //
	// - menuFirst: menu qui doit etre affiché quand rien n'est demander - //
	function show($menuFisrt=NULL){
		if($menuFisrt===NULL)$menuFisrt=$this->menuDefaut;

		// - afficher le 1er menu si aucun menu definit dans le get - //
		if(($this->menuLvl==1) and (isset($this->menus[$menuFisrt])) ) echo $out=$this->menus[$menuFisrt]->showMenu($this);

		// - afficher le menu construit avec build() - //
		echo $this->_buildData;

		// - afficher la page actif du dernier menu - //
		$this->showPageContent();
	}
	
	function showPageContent($showPage=NULL){
		if($showPage!==NULL)$this->showPage=$showPage;
		//echo gestLib_inspect('$showPage',$showPage);

		// si mode auto et que menu appeller dans le query
		if($this->showPage===1 AND $this->isCall===1){
			//echo 'affichage de la page';
			//echo gestLib_inspect('$this->menus[$this->_pageSelect]',$this->menus[$this->_pageSelect]);
			echo '<div id="'.$this->pageContentId.'">';
			if(isset($this->menus[$this->_pageSelect])){
				//echo gestLib_inspect('$this->_pageSelect',$this->_pageSelect);
				$this->menus[$this->_pageSelect]->incPage();
			}
			echo '</div> <!--div id="$this->pageContentId"-->';
		}
	}

} // class gestMenus


// -- *********************************************************************** -- //
// -- Fin de la lib -- //
// -- *********************************************************************** -- //
$gestLib->setEtat('gestMenus',LEGRAL_LIBETAT::LOADED);
//$gestMenus=new gestMenus(); // creation d'une instance 

$gestLib->end('gestMenus');
?>
