<?php
//=== SYSLinux - les annexes === //

$mn='SYSLinux-annexes';
$pagePath=PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/SYSLinux/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
	$m->setAttr($p,'titre','SYSLinux - annexes');
        $m->setAttr($p,'menuTitre','SYSLinux:annexes');

$p='repFile';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - annexes - structures des r&eacute;pertoires et contenus des fichiers');
        $m->setAttr($p,'menuTitre','R&eacute;pertoires et fichiers');

$p='whereIsKernel';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','Annexes - Ou trouver les kernels et initrd');
        $m->setAttr($p,'menuTitre','kernel et initrd');

$p='manlive-boot';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','man:live-boot');
        $m->setAttr($p,'menuTitre','man:live-boot');

$p='manlive-build';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','man:live-build');
        $m->setAttr($p,'menuTitre','man:live-build');

$p='manlive-config';
 $m->addCallPage($p,$pagePath."$p.html");
       $m->setAttr($p,'titre','man: live-config');
        $m->setAttr($p,'menuTitre','man: live-config');
?>
