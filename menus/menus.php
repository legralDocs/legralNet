<?php
// ==== menu: primaire ==== //

$mn='tutoriels';
$pagePath=PAGESLOCALES_ROOT."legralNet/";

$p='accueil';	// cette opage est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath.'tutoriels/accueil.html');
	// -- parametrer la page -- //
	$m->setAttr($p,'visible',1);				// 0: le li ne sera pas affiche 1:afficher
	$m->setAttr($p,'menuTitre','tutoriels');		// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
	$m->setAttr($p,'menuTitle','tutoriels - accueil');	// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
	$m->setAttr($p,'titre','accueil(bas de page)');	// titre de la page: afficher dans le bas de page
//      $m->setMeta($p,'title','tutoriels - accueil(meta)');	// meta <title> (si non definit title=titre)
//	$m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>
	
$p='pageModele'; // cette page est aussi un menu
$m->addCallPage($p,$pagePath.'pageModele.html');
	$m->setAttr($p,'visible',0);
	$m->setAttr($p,'menuTitre',"pageModèle");
	$m->setAttr($p,'titre',"page modèle");

$p='bootLoader'; // cette page est aussi un menu
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
	$m->setAttr($p,'menuTitre',"bootLoader");
	$m->setAttr($p,'titre',"bootLoader: Amorcer son syst&eacute;me");
	$m->addCssA($p,'dossier1');

$p='linux';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre',"GNU/Linux");
        $m->setAttr($p,'titre',"GNU/Linux Debian");
	$m->addCssA($p,'dossier1');


$p='internet';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$p: protocole et programme");
        $m->addCssA($p,'dossier1');


$p='programmations';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
	$m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$p: programmations langage(PHP,PYTHON,Javascript,HTML,CSS) et compilations");
        $m->addCssA($p,'dossier1');
/*	
$p='langages';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
	$m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$p: PHP,PYTHON,Javascript,HTML,CSS");
        $m->addCssA($p,'dossier1');
 */	
$p='logiciels';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
	$m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$p: logiciels divers et variés");
        $m->addCssA($p,'dossier1');

$p='annexes';
$m->addCallPage($p,$pagePath."tutoriels/$p.html");
	$m->setAttr($p,'menuTitre',"annexes");
        $m->setAttr($p,'titre',"$mn: annexes");

include('./menus/menus-bootLoader.php');
include('./menus/menus-linux.php');
include('./menus/menus-internet.php');
include('./menus/menus-programmations.php');
include('./menus/menus-langages.php');
include('./menus/menus-logiciels.php');

?>
