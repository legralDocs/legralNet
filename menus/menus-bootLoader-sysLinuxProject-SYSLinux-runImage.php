<?php
// ==== menu: SYSLinux: lancer une image  ==== //

$mn='SYSLinux-runImage';
$pagePath=PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/SYSLinux/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - lancer une image');
        $m->setAttr($p,'menuTitre','lancer une image');

$p='lancer-memDisk';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - lancer une image via le module memDisk');
        $m->setAttr($p,'menuTitre','memDisk');

$p='lancer-exemple';
	$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - exemple de lancement d\'images');
        $m->setAttr($p,'menuTitre','exemples');
	
$p='aideEnVrac';
	$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - texte/source d\'aide en vrac (non test&eacute;)');
        $m->setAttr($p,'menuTitre','aide En Vrac');

$p='exempleMenu-legacy';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - ancienne syntaxe');
        $m->setAttr($p,'menuTitre','ancienne syntaxe');

$p='append-linux';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - les options pour append pour modifier le kernel linux');
        $m->setAttr($p,'menuTitre','append');

/*

//===  sous menu: SYSLinux - les annexes === //
$p='SYSLinux-annexes_accueil';
$m=$gestMenus->addMenu('SYSLinux-annexes',$p,PAGES_ROOT.'legralNet/sysLinuxProject/SYSLinux/annexe-accueil.html');
        $m->setAttr($p,'titre','SYSLinux - annexes');
        $m->setAttr($p,'menuTitre','SYSLinux:annexes');

$p='SYSLinux-annexes_repFile';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/SYSLinux/annexe-repFile.html');
        $m->setAttr($p,'titre','SYSLinux - annexes - structures des r&eacute;pertoires et contenus des fichiers');
        $m->setAttr($p,'menuTitre','R&eacute;pertoires et fichiers');

$p='SYSLinux-annexes_whereIsKernel';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/SYSLinux/annexe-whereIsKernel.html');
        $m->setAttr($p,'titre','Annexes - Ou trouver les kernels et initrd');
        $m->setAttr($p,'menuTitre','kernel et initrd');

$p='SYSLinux-manlive-boot';
$m->addCallPage($p,PAGES_ROOT.'legralNet/debianLive/docs/live-boot.html');
        $m->setAttr($p,'titre','man:live-boot');
        $m->setAttr($p,'menuTitre','man:live-boot');

$p='SYSLinux-manlive-build';
$m->addCallPage($p,PAGES_ROOT.'legralNet/debianLive/docs/live-build.html');
        $m->setAttr($p,'titre','man:live-build');
        $m->setAttr($p,'menuTitre','man:live-build');

$p='SYSLinux-manlive-config';
$m->addCallPage($p,PAGES_ROOT.'legralNet/debianLive/docs/live-config.html');
        $m->setAttr($p,'titre','man: live-config');
        $m->setAttr($p,'menuTitre','man: live-config');


// ==== menu:PXELinux  ==== //
$p='';
$p='PXELinux-accueil';
$m=$gestMenus->addMenu('PXELinux',$p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/PXELinux-accueil.html');
        $m->setAttr($p,'menuTitre','PXELinux');
        $m->setAttr($p,'titre',"PXELinux");

$p='PXELinux-utilisation';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/utilisation.html');
        $m->setAttr($p,'menuTitre','utilisation Avantage/inconv&eacute;nient');
        $m->setAttr($p,'titre',"SYSLinux: utilisation Avantage et inconv&eacute;nient");

$p='PXELinux-demonstration';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/demonstration.html');
	$m->setAttr($p,'titre',"analyse étape par étape d'un boot");
	$m->setAttr($p,'menuTitre','d&eacute;monstration');

$p='PXELinux-mecanisme';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/mecanisme.html');
	$m->setAttr($p,'titre','PXELinux: m&eacute;canisme');
	$m->setAttr($p,'menuTitre','m&eacute;canisme');

$p='PXELinux-install';
$m->addCallPage($p,'');
	$m->setAttr($p,'titre','installations');
	$m->setAttr($p,'menuTitre','installations');
        $m->addCssA($p,'dossier1');

$p='PXELinux-outils';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/outils.html');
	$m->setAttr($p,'titre','outils');
	$m->setAttr($p,'menuTitre','outils');

$p='PXELinux-references';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/references.html');
	$m->setAttr($p,'titre','r&eacute;f&eacute;rences');
	$m->setAttr($p,'menuTitre','r&eacute;f&eacute;rences');

// ==== menu:PXELinux-installation  ==== //
$p='PXELinux-installation';
$m=$gestMenus->addMenu('PXELinux-install',$p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/install.html');
        $m->setAttr($p,'titre','PXELinux: installation');
        $m->setAttr($p,'menuTitre','installations');

$p='PXELinux-installation-dhcp';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/install-dhcp.html');
        $m->setAttr($p,'titre','PXELinux: configuration du serveur dhcp');
        $m->setAttr($p,'menuTitre','dhcp');

$p='PXELinux-installation-tftp';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/install-tftp.html');
        $m->setAttr($p,'titre','configuration du serveur tftp');
        $m->setAttr($p,'menuTitre','tftp');

$p='PXELinux-installation-pxe';
$m->addCallPage($p,PAGES_ROOT.'legralNet/sysLinuxProject/PXELinux/install-pxe.html');
        $m->setAttr($p,'titre','configuration du serveur pxe');
	$m->setAttr($p,'menuTitre','pxe');
 */
?>
