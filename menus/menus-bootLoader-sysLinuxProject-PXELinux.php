<?php
// ==== menu: PXELinux  ==== //

$mn='PXELinux';
$pagePath=PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre',$mn);
        $m->setAttr($p,'titre',"$mn");

$p='utilisation';
$m->addCallPage($p,$pagePath."$p.html");
$m->setAttr($p,'menuTitre','utilisation Avantage/inconv&eacute;nient');
        $m->setAttr($p,'titre',"SYSLinux: utilisation Avantage et inconv&eacute;nient");

$p='demonstration';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"analyse étape par étape d'un boot");
	$m->setAttr($p,'menuTitre','d&eacute;monstration');

$p='mecanisme';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre','PXELinux: m&eacute;canisme');
	$m->setAttr($p,'menuTitre','m&eacute;canisme');

$p='PXELinux-installation';
//$m->addCallPage($p,$pagePath."$p.html");
$m->addCallPage($p,$pagePath."install.html");
	$m->setAttr($p,'titre','installations');
	$m->setAttr($p,'menuTitre','installations');
        $m->addCssA($p,'dossier1');

$p='outils';
$m->addCallPage($p,$pagePath."$p.html");
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre','outils');
	$m->setAttr($p,'menuTitre','outils');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$mn: $p");

// ==== menu:PXELinux-installation  ==== //
	
//$mn='PXELinux-installation'; // ne pas surcharger ici
$pagePath=PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/$mn/";
$mn='PXELinux-installation'; // mais surcharger ici!

$p='install'; //accueil
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','PXELinux: installation');
        $m->setAttr($p,'menuTitre','installations');

$p='install-dhcp';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','PXELinux: configuration du serveur dhcp');
        $m->setAttr($p,'menuTitre','dhcp');

$p='install-tftp';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','configuration du serveur tftp');
        $m->setAttr($p,'menuTitre','tftp');

$p='install-pxe';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','configuration du serveur pxe');
        $m->setAttr($p,'menuTitre','pxe');
?>
