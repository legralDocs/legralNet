<?php
// ==== menu: SYSLinux  ==== //

$mn='SYSLinux';
$pagePath=PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','SYSLinux');
        $m->setAttr($p,'titre','SYSLinux');

$p='exempleMenu-legacy';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'visible',0);
        $m->setAttr($p,'menuTitre',"$mn: exemple minimal");
        $m->setAttr($p,'title',"$mn:exemple de menu minimaliste");

$p='install';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre',"installation");
        $m->setAttr($p,'titre',"$mn:installer sysLinux sur un disque");

$p='parametresGeneraux';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','param&egrave;tres G&eacute;n&eacute;raux');
        $m->setAttr($p,'titre',"$mn: param&egrave;tres G&eacute;n&eacute;raux");

$p='SYSLinux-runImage';
$m->addCallPage($p,$pagePath."$p/accueil.html");
        $m->setAttr($p,'titre','SYSLinux - lancer une image');
        $m->setAttr($p,'menuTitre','lancer une image');
	$m->addCssA($p,'dossier1');

$p='SYSLinux-UI';
$m->addCallPage($p,$pagePath."$p/accueil.html");
        $m->setAttr($p,'titre',"l'interface graphique");
        $m->setAttr($p,'menuTitre',"l'interface graphique");
	$m->addCssA($p,'dossier1');

$p='SYSLinux-annexes';
$m->addCallPage($p,$pagePath."$p/accueil.html");
        $m->setAttr($p,'titre','annexes');
        $m->setAttr($p,'menuTitre','annexes');
	$m->addCssA($p,'dossier1');

// - inclusion des sous menus - //	
include('./menus/menus-bootLoader-sysLinuxProject-SYSLinux-runImage.php');
include('./menus/menus-bootLoader-sysLinuxProject-SYSLinux-UI.php');
include('./menus/menus-bootLoader-sysLinuxProject-SYSLinux-annexes.php');
?>
