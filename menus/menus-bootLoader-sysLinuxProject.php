<?php
// ==== menu: sysLinuxProject ==== //

$mn='sysLinuxProject';
$pagePath=PAGES_ROOT."legralNet/bootLoader/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','sysLinuxProject');
        $m->setAttr($p,'titre',"$mn: logiciel d'amorcage sur disque et via le LAN");

$p='SYSLinux';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','SYSLinux');
        $m->setAttr($p,'titre',"SYSLinux");
        $m->addCssA($p,'dossier1');

$p='PXELinux';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','PXELinux');
        $m->setAttr($p,'titre',"PXELinux");
        $m->addCssA($p,'dossier1');

// - inclusion des sous menus - //	
include('./menus/menus-bootLoader-sysLinuxProject-SYSLinux.php');
include('./menus/menus-bootLoader-sysLinuxProject-PXELinux.php');


?>
