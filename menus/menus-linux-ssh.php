<?php
// ==== menu: GNU/Linux  ==== //

$mn='ssh';
$pagePath=PAGES_ROOT."legralNet/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','ssh:Secure  Shell');
        $m->setAttr($p,'menuTitre','ssh');

	
$p='ssh';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre',"GNU/Linux: le kernel");
        $m->setAttr($p,'menuTitre','le kernel');
	$m->addCssA($p,'dossier1');

$p='linux-fs';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre','GNU/Linux: les systèmes de fichiers');
        $m->setAttr($p,'menuTitre','les systèmes de fichiers');

$p='linux-distributions';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre',"GNU/Linux: les distributions");
        $m->setAttr($p,'menuTitre','les distributions');
	$m->addCssA($p,'dossier1');

include('./menus/menus-linux-kernel.php');
include('./menus/menus-linux-fs.php');
include('./menus/menus-linux-distributions.php');

?>
