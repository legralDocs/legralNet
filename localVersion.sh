#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier


#######################################
# updateModele()                        #
# met les lib a jours (par linkage) #
#######################################
updateModele(){
	echo "$couleurINFO # - localVersion.sh:updateModele() - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		echo "";
		echo "$couleurINFO   $couleurNORMAL";
		cmd="mkdir -p ./scripts/;"eval $cmd;
		cmd="mkdir -p ./styles";  eval $cmd;
		cmd="mkdir -p ./config";  eval $cmd;
		cmd="mkdir -p ./configLocal";  eval $cmd;
		
		# -- gestionnaire de versions -- #
		echo "";
		echo "$couleurINFO gestionnaire de versions $couleurNORMAL";
		echo "";
		echo "";
		lib='gitVersion';		# -attention lib non formalise -#

		#version dev
		libV="$lib.sh";cmd="link /www/git/bash/$lib/scripts/$libV ./scripts/$libV";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		#version fixe
		#libV="$lib-v2.0.1.sh";cmd="link /www/git/bash/$lib/versions/scripts/$libV ./scripts/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;

		echo "";
		echo "# -- librairies  -- #";
		echo "";
		echo "$couleurINFO librairies $couleurNORMAL";
		lib='gestLib';
		cmd="mkdir -p ./lib/legral/php/$lib";eval $cmd;
		#version dev
		libV="$lib.php";cmd="link /www/git/intersites/lib/legral/php/$lib/$libV	./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		libV="$lib.js"; cmd="link /www/git/intersites/lib/legral/php/$lib/$libV	./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;
		libV="$lib.css";cmd="link /www/git/intersites/lib/legral/php/$lib/$libV	./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		#version fixe
		#libV="$lib-v2.0.0.php";cmd="cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;";  echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;
		#libV="$lib-v2.0.0.js" ;cmd="cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;";  echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;
		#libV="$lib-v2.0.0.css";cmd="cp /www/git/intersites/lib/legral/php/$lib/versions/$libV   ./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;

		lib='gestMenus';
		cmd="mkdir -p ./lib/legral/php/$lib";eval $cmd;
		libV="$lib.php";cmd="link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		libV="$lib.css";cmd="link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		lib='menuStylisee';
		libV="$lib.php";cmd="link /www/git/intersites/lib/legral/php/gestMenus/$libV ./lib/legral/php/gestMenus/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		libV="$lib.css";cmd="link /www/git/intersites/lib/legral/php/gestMenus/$libV ./lib/legral/php/gestMenus/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		lib='gestPDO';
		cmd="mkdir -p ./lib/legral/php/$lib";eval $cmd;
		libV="$lib.php";cmd="link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";  eval $cmd;
		#libV="$lib-v1.1.0.php";cmd="cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		libV="$lib-erreurs.php";cmd="link /www/git/intersites/lib/legral/php/$lib/config/$libV ./config/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;
		libV="configDB.php";cmd="cp --no-clobber /www/git/intersites/lib/legral/php/$lib/config/$libV ./config/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;
		lib='gestTables';
		libV="$lib.php";cmd="link /www/git/intersites/lib/legral/php/gestPDO/$libV ./lib/legral/php/gestPDO/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";  eval $cmd;

		lib='gestLogins';
		cmd="mkdir -p ./lib/legral/php/$lib";eval $cmd;
		libV="$lib.php";cmd="link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;
		libV="configLogins.php";cmd="cp --no-clobber /www/git/intersites/lib/legral/php/$lib/config/$libV ./config/$libV;";echo "$couleurINFO $cmd $couleurNORMAL"; eval $cmd;

		#lib='gestClasseurs';
		#cmd="mkdir -p ./lib/legral/js/$lib";eval $cmd;
		#version dev
		#libV="$lib.js"; cmd="link /www/git/intersites/lib/legral/js/$lib/$libV ./lib/legral/js/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";  eval $cmd;
		#libV="$lib.css";cmd="link /www/git/intersites/lib/legral/js/$lib/$libV ./lib/legral/js/$lib/$libV;";echo "$couleurINFO $cmd $couleurNORMAL";  eval $cmd;

		# -- styles communs -- #
		echo "";
		echo "$couleurINFO styles communs  $couleurNORMAL";
		css='knacss';
		mkdir -p ./styles/$css;
		cssV=$css'.css';        cmd="cp  /www/git/gitModele/styles/$css/$cssV ./styles/$css/$cssV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		#cssV=$css'.css-V4.1.6';cmd="cp  /www/git/gitModele/styles/$css/$cssV ./styles/$css/$cssV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		css='';
		cssV=$css'html4.css';cmd="link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		css='';
		cssV=$css'intersites.css';cmd="link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		css='notes';
		mkdir -p ./styles/$css;
		cmd="cp -R --no-clobber /www/git/gitModele/styles/$css ./styles/;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		cssV=$css'Relatif.css';
		#cmd="rm ./styles/$css/$cssV";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;		#dessactiver pour le modele!
		cmd="link /www/git/gitModele/styles/$css/$cssV ./styles/$css/$cssV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		css='tutoriels';
		cmd="cp -R --no-clobber /www/git/gitModele/styles/$css ./styles/;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		cssV=$css'Relatif.css';
		#cmd="rm ./styles/$css/$cssV";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;	#dessactiver pour le modele!
		cmd="link /www/git/gitModele/styles/$css/$cssV ./styles/$css/$cssV;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		# -- menus communs -- #
		echo "";
		echo "$couleurINFO menus communs $couleurNORMAL";
		cmd="mkdir -p ./menus/";eval $cmd;
		f='menus/menus-systeme.php';cmd="link /www/git/gitModele/$f $f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		echo "";
		echo "$couleurINFO pages communes $couleurNORMAL";
		cmd="mkdir -p ./pagesLocales/_site/";eval $cmd;
		f='_site/about.php';  cmd="link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='_site/credits.php';cmd="link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='_site/inspect.php';cmd="link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='_site/plans.php';  cmd="link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;


		# -- fichiers communs -- #
		echo "";
		echo "$couleurINFO fichiers communs $couleurNORMAL";eval $cmd;
		f='localVersion.sh';cmd="cp --no-clobber /www/git/gitModele/$f ./$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='index.php';cmd="cp --no-clobber /www/git/gitModele/$f ./$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='piwik.php';cmd="link /www/git/gitModele/$f ./$f;";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		# -- Changement des droits  -- #
		echo "";
		echo "$couleurINFO Changement des droits $couleurNORMAL";
		f='./';	cmd="chmod -R 755 $f";echo "$couleurWARN $cmd (déactivé par défaut)$couleurNORMAL"; #eval $cmd;
		f='./scripts/';	cmd="chmod -R 755 $f";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./styles/';	cmd="chmod -R 755 $f";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./menus/';	cmd="chmod -R 755 $f";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./lib/';	cmd="chmod -R 755 $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./pagesLocales/';cmd="chmod -R 755 $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
	fi
}


#########################################################
# localConcat()                                         #
# concatenne les fichiers css dans ./styles/styles.css  #
# concatenne les fichiers js  dans ./locales/scripts.js #
#########################################################
localConcat() {
        # - concatenation des fichiers css - #
        echo "$couleurINFO # - concatenation des fichiers css dans ./styles/styles.css - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		f='./styles/knacss/knacss.css';		cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		f='./styles/html4.css';			cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./styles/intersites.css';		cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		f='./lib/legral/php/gestLib/gestLib.css';cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
	
		f='./lib/legral/php/gestMenus/menuStylisee.css';cmd="catCSS $f";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./lib/legral/php/gestMenus/gestMenus.css';cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		f='./styles/notes/notesRelatif.css';	cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		f='./styles/tutoriels/tutorielsRelatif.css';	cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
	fi

        # - concatenation des fichiers js - #
	echo "";
        echo "$couleurINFO # - concatenation des fichiers js dans ./locales/scripts.js  - #$couleurNORMAL";
	#echo "$couleurWARN Pas de lib js à concatener $couleurNORMAL";return 1;	# a commenter une fois parametré
	if [ $isSimulation -eq 0 ];then
		#echo "$couleurWARN Pas de lib js à concatener $couleurNORMAL";return 1;	# a commenter une fois parametré
		#f='/www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js';cmd="catJS $f";	echo "$couleurINFO $cmd $couleurNORMAL";catJS $f;#eval $cmd;
		f='/www/git/intersites/lib/legral/php/gestLib/gestLib.js';		cmd="catJS $f";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
		#f='/www/git/intersites/lib/legral/js/gestClasseurs/gestClasseurs.js';	cmd="catJS $f";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
	fi
}


##################################################
# localSave()                                    #
# copie un fichier en le suffixant de la version #
##################################################
localSave() {
	echo "$couleurINFO versionning des fichiers locaux$couleurNORMAL";
	echo "$couleurWARN Rien à versionner $couleurNORMAL";return 1;	# a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		echo "$couleurWARN Rien à versionner $couleurNORMAL";return 1;	# a commenter une fois parametré

		# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
		#cmd="versionSave ./scripts/gitVersion sh";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

	fi
	}


#######################################
# localStatification()                #
# télécharge et rend statique un site #
#######################################
localStatification(){
	echo "$couleurINFO # - localVersion.sh:localStatification() - #$couleurNORMAL";
	lib='legralNet';
	url="http://127.0.0.1/git/legralDocs/$lib";

	echo "telechargement avec rendu statique d'un site de $url";
	#echo "$couleurWARN Rien à statifier $couleurNORMAL";return 1;	# a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		
		#echo "$couleurINFO suppression et creation de ../statique/$couleurNORMAL";
		cmd="rm -R ./statique/;mkdir ./statique/;cd   ./statique/;"
		echo "$couleurINFO $cmd $couleurNORMAL";
		eval $cmd;

		echo "$couleurINFO téléchargement...$couleurWARN";
		echo "$couleurINFO la version statique se trouvera dans : $url/statique $couleurNORMAL";

		#--no-verbose --quiet
		cmd="wget --quiet --tries=5 --continue --no-host-directories --html-extension --recursive --level=inf --convert-links --page-requisites --no-parent --restrict-file-names=windows --random-wait --no-check-certificate $url";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		echo "$couleurINFO deplacer et nettoyer le chemin$couleurNORMAL";
		cmd="mv ./git/legralDocs/$lib ./";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

		# - decommenter pour activer la suppression apres verification - #
		cmd="rm -R ./git/";echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
	fi
}


#######################################
# syncRemote()                        #
# synchronise le(s) serveurs distants #
#######################################
syncRemote(){
	#echo "$couleurWARN # - pas de Remote - #$couleurNORMAL";return 1; # a commenter une fois parametré
	lib='legralNet';
	urlDest="legralDocs/$lib";
	echo "$couleurINFO url: http://legral.fr/$urlDest $couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		#echo "$couleurWARN # - pas de Remote - #$couleurNORMAL";return 1; # a commenter une fois parametré
		#echo "exemple:";
		#lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./statique/$lib   /www/$urlDest ; quit";
		cmd="lftp -u legral ftp://ftp.legral.fr -e 'mirror -e -R  ./statique/$lib   /www/$urlDest ; quit';"
		echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;
	fi
}


#################
# postGit()     #
# lancer en fin #
#################
postGit(){
	echo "$couleurINFO # - localVersion.sh:postGit() - #$couleurNORMAL";
	echo "$couleurWARN Pas de postGit $couleurNORMAL";return 1; # a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		echo "$couleurWARN Pas de postGit $couleurNORMAL";return 1; # a commenter une fois parametré
	fi
}
